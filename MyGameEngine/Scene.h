#pragma once
#include "Global.h"
#include "Sprite.h"
#include "Label.h"
#include "Node.h"
#include "Quad.h"
#include "Fbx.h"
#include <vector>

class Camera;
class Scene
{
protected:
	std::vector<Node*> _nodes;
	Camera* _camera;


	////////////////////////////////////////////////////////////////////////////////////////////////
	//サブシーン
	enum SUB_SCENE
	{
		SUB_SCENE_FEAD_IN,		//フェードイン中
		SUB_SCENE_DEFAULT,		//通常
		SUB_SCENE_FEAD_OUT,		//フェードアウト中
	}_subScene;	//現在のサブシーン

	static Sprite*	_fadeBlack;		//フェードイン・アウト用の真っ黒のスプライト（すべてのシーンで共通だからstatic）
	int		_fadeAlpha;				//真っ黒スプライトのアルファ値
	////////////////////////////////////////////////////////////////////////////////////////////////


public:
	Scene();
	virtual ~Scene();
	void addChild(Node* pNode);
	void draw();
	void removeChild(Node* pNode);

	virtual void init() = 0;
	virtual void update();
	virtual void input();

	////////////////////////////////////////////////////////////////////////////////////////////////
	//フェードアウトスタート
	void FadeOutStart();

	//真っ黒スプライトのロード
	static void LoadFeadBlack();

	//真っ黒スプライト開放
	static void FreeFadeBlack();
	////////////////////////////////////////////////////////////////////////////////////////////////
};

