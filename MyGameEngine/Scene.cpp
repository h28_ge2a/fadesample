#include "Scene.h"
#include "Camera.h"

Sprite*	Scene::_fadeBlack = nullptr;

Scene::Scene()
{
	auto camera = Camera::create();
	this->addChild(camera);
	_camera = camera;


	////////////////////////////////////////////////////////////////////////////////////////////////
	_fadeAlpha = 255;
	_subScene = SUB_SCENE_FEAD_IN;
	////////////////////////////////////////////////////////////////////////////////////////////////

}


Scene::~Scene()
{
	for (int i=0; i < _nodes.size(); i++)
	{
		SAFE_DELETE	(_nodes[i]);
	}
}

void Scene::addChild(Node* pNode)
{
	pNode->setParent(this);
	_nodes.push_back(pNode);
}

void Scene::update()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->update();
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	//フェードイン・アウト処理
	switch (_subScene)
	{
	case SUB_SCENE_FEAD_IN:	//フェードイン中

		//だんだん透明に
		_fadeAlpha -= 5;						
		_fadeBlack->setAlpha(_fadeAlpha);

		//透明になったらサブシーンをデフォルトに
		if (_fadeAlpha <= 0)
		{
			_subScene = SUB_SCENE_DEFAULT;
		}
		break;


	case SUB_SCENE_FEAD_OUT:	//フェードアウト中

		//だんだん濃く
		_fadeAlpha += 5;
		_fadeBlack->setAlpha(_fadeAlpha);

		//不透明になったらシーンを切り替える
		if (_fadeAlpha >=255)
		{
			g.changeScene();
		}
		break;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////


}


//入力処理
void Scene::input()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->input();
	}
}

//描画処理
void Scene::draw()
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->draw(); 	
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	//フェード用真っ黒画像を描画
	if (_subScene != SUB_SCENE_DEFAULT)
	{
		_fadeBlack->draw();
	}
	////////////////////////////////////////////////////////////////////////////////////////////////
}

//ノードの削除関数
void Scene::removeChild(Node* pNode)
{
	for (int i = 0; i < _nodes.size(); i++)
	{
		if (pNode == _nodes[i])
		{
			SAFE_DELETE(_nodes[i]);
			_nodes.erase(_nodes.begin() + i);
			break;
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////
//フェードアウトスタート
void Scene::FadeOutStart()
{
	_subScene = SUB_SCENE_FEAD_OUT;
}


//真っ黒スプライトのロード
void Scene::LoadFeadBlack()
{
	_fadeBlack = new Sprite();
	_fadeBlack->load("Assets\\fadeBlack.png");
	_fadeBlack->setPosition(g.WINDOW_WIDTH / 2, g.WINDOW_HEIGHT / 2);	//ウィンドウの中央に配置
	_fadeBlack->setScale(4, 3);											//適当に拡大（最初から大きい絵を用意してもいいけど）
}

//真っ黒スプライト開放
void Scene::FreeFadeBlack()
{
	SAFE_DELETE(_fadeBlack);
}
////////////////////////////////////////////////////////////////////////////////////////////////

