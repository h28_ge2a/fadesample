#include "Global.h"
#include "Scene.h"


//シーン切り替え（フェードアウトし始める）
//引数：pNextScene	次のシーンのオブジェクト
void Global::replaceScene(Scene* pNextScene)
{
	////////////////////////////////////////////////////////////////////////////////////////////////
	_nextScene = pNextScene;	//次のシーンを覚えておく
	pScene->FadeOutStart();
	////////////////////////////////////////////////////////////////////////////////////////////////
}



////////////////////////////////////////////////////////////////////////////////////////////////
//本当にシーン切り替え
void Global::changeScene()
{
	delete pScene; //現在のシーンを開放

	pScene = _nextScene;	//シーンを変えて
	pScene->init();			//新しいシーンの初期化

}
////////////////////////////////////////////////////////////////////////////////////////////////