#pragma once
#include "Global.h"
#include "MyRect.h"
#include "Node.h"

class Sprite:public Node{
	LPD3DXSPRITE _pSprite;			//スプライト
	LPDIRECT3DTEXTURE9  _pTexture;  //テクスチャ
	MyRect     _cut;				//切抜き範囲
	int			_alpha;				//アルファ値
	
public:
	Sprite();
	~Sprite();
	
	static Sprite* create(LPCSTR fileName, MyRect cut = MyRect(-999, -999, -999, -999)); 
	void Sprite::load(LPCSTR fileName, MyRect cut = MyRect(-999, -999, -999, -999));
	void Sprite ::draw();
	void Sprite ::load();

	void setRect(MyRect cut);
	void setAlpha(int alpha);
};

