#pragma once
#include  <string>
#include "Global.h"

//Create関数生成マクロ
#define CREATE_FUNC(CLASS_NAME)	\
	static CLASS_NAME* create()	\
{	\
	CLASS_NAME* p = new CLASS_NAME;	\
	p->init();	\
	return p;	\
}


class Node
{
protected:
	D3DXVECTOR3 _size;			//サイズ
	D3DXVECTOR3 _anchorPoint;	//アンカーポイント
	D3DXVECTOR3 _position;	    //位置
	D3DXVECTOR3 _rotate;	    //回転角度
	D3DXVECTOR3 _scale;			//拡大率

	Scene* _parent;		//親シーン
	std::string _tag;	//タグ


public:
	Node();
	virtual ~Node();
	virtual void input(){}
	virtual void update(){}
	virtual void draw(){};

	void removeFromParent();	//自身を削除

	//各セッター
	void setAnchorPoint(float x, float y, float z = 0); //アンカーポイント（基準点)
	void setPosition(float x, float y, float z = 0);	//位置
	void setPosition(D3DXVECTOR3 pos);

	void setRotate(float x, float y, float z);			//回転角度
	void setRotate(float z);							//回転角度(回転しない方向)
	void setScale(float x, float y, float z = 1);		//拡大倍率
	void  setParent(Scene* parent);		//親シーン
	void setTag(std::string  tag);		//タグ
	MyRect  getBoundingBox();			//衝突判定
		
	//各ゲッター
	D3DXVECTOR3 getSize();			//サイズ
	D3DXVECTOR3 getAnchorPoint();	//アンカーポイント
	D3DXVECTOR3 getPosition();		//位置
	D3DXVECTOR3 getRotate();		//回転角度
	D3DXVECTOR3 getScale();			//倍率
	std::string getTag();			//タグ
};

