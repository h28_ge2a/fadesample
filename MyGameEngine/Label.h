#pragma once
#include "Global.h"
#include "Node.h"

class Label:public Node 
{
	LPD3DXFONT	_pFont;			//フォント
	TCHAR		_string[256];    //表示する文字列
public:
	Label();
	~Label();
	static Label* create(LPCSTR label, LPCSTR font, int _scale); //作成
	void load(LPCSTR font,int size);//フォント
	void draw();					//描画
	void setString(LPCSTR str);		//表示する文字列の変更
};

