#include "PlayScene.h"
#include "TitleScene.h"

PlayScene::PlayScene()
{

}


PlayScene::~PlayScene()
{

}

void PlayScene::init()
{
	auto logo = Label::create("プレイシーン", "ＭＳ ゴシック", 64);
	logo->setPosition(g.WINDOW_WIDTH / 2, g.WINDOW_HEIGHT / 2);
	this->addChild(logo);

	auto push = Label::create("スペースキーを押してください", "ＭＳ ゴシック", 32);
	push->setPosition(g.WINDOW_WIDTH / 2, g.WINDOW_HEIGHT * 3 / 4);
	this->addChild(push);
}


void PlayScene::input()
{
	Scene::input();

	if (g.pInput->isKeyTap(DIK_SPACE))
		g.replaceScene(new TitleScene);
}

