#include "TitleScene.h"
#include "PlayScene.h"

TitleScene::TitleScene()
{

}


TitleScene::~TitleScene()
{

}

void TitleScene::init()
{
	auto logo = Label::create("タイトルシーン", "ＭＳ ゴシック", 64);
	logo->setPosition(g.WINDOW_WIDTH / 2, g.WINDOW_HEIGHT / 2);
	this->addChild(logo);

	auto push = Label::create("スペースキーを押してください", "ＭＳ ゴシック", 32);
	push->setPosition(g.WINDOW_WIDTH / 2, g.WINDOW_HEIGHT * 3 / 4);
	this->addChild(push);
}


void TitleScene::input()
{
	Scene::input();

	if(g.pInput->isKeyTap(DIK_SPACE))
		g.replaceScene(new PlayScene);
}

