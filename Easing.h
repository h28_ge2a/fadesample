#include <d3dx9.h>

class Easing
{
public:
	static D3DXVECTOR3 Linear
		(D3DXVECTOR3 start,
		D3DXVECTOR3 goal,
		float duration,
		float time)
	{
		D3DXVECTOR3 v = goal - start;

		if (time > duration)
			time = duration;

		time /= duration;
		return v * time + start;
	}

	static D3DXVECTOR3 In
		(D3DXVECTOR3 start,
		D3DXVECTOR3 goal,
		float duration,
		float time)
	{
		D3DXVECTOR3 v = goal - start;

		if (time > duration)
			time = duration;

		time /= duration;
		return v * pow(time, 2) + start;
	}

	static D3DXVECTOR3 Out
		(D3DXVECTOR3 start,
		D3DXVECTOR3 goal,
		float duration,
		float time)
	{
		D3DXVECTOR3 v = goal - start;

		if (time > duration)
			time = duration;

		time /= duration;
		return -v * time * (time - 2) + start;
	}

	static D3DXVECTOR3 InOut
		(D3DXVECTOR3 start,
		D3DXVECTOR3 goal,
		float duration,
		float time)
	{
		D3DXVECTOR3 v = goal - start;

		if (time > duration)
			time = duration;

		time /= duration / 2;
		if (time < 1)
			return v / 2 * pow(time, 2) + start;
		else
			return -v / 2 * (--time * (time - 2) - 1) + start;
	}
};